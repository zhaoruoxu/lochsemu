#include "stdafx.h"
#include "processor.h"
#include "inst_table.h"

BEGIN_NAMESPACE_LOCHSEMU()

static InstHandler Fpu_Ext_D8_Handlers[] = 
{
    /* 0x00 */Fpu_Fadd32fp_D8_0,
    /* 0x08 */InstNotAvailable,
    /* 0x10 */InstNotAvailable,
    /* 0x18 */InstNotAvailable,
    /* 0x20 */InstNotAvailable,
    /* 0x28 */InstNotAvailable,
    /* 0x30 */Fpu_Fdiv32fp_D8_6,
    /* 0x38 */InstNotAvailable,

    /* 0xC0 */Fpu_Fadd_D8C0,
    /* 0xC1 */Fpu_Fadd_D8C0,
    /* 0xC2 */Fpu_Fadd_D8C0,
    /* 0xC3 */Fpu_Fadd_D8C0,
    /* 0xC4 */Fpu_Fadd_D8C0,
    /* 0xC5 */Fpu_Fadd_D8C0,
    /* 0xC6 */Fpu_Fadd_D8C0,
    /* 0xC7 */Fpu_Fadd_D8C0,
    /* 0xC8 */Fpu_Fmul_D8C8,
    /* 0xC9 */Fpu_Fmul_D8C8,
    /* 0xCA */Fpu_Fmul_D8C8,
    /* 0xCB */Fpu_Fmul_D8C8,
    /* 0xCC */Fpu_Fmul_D8C8,
    /* 0xCD */Fpu_Fmul_D8C8,
    /* 0xCE */Fpu_Fmul_D8C8,
    /* 0xCF */Fpu_Fmul_D8C8,
    /* 0xD0 */InstNotAvailable,
    /* 0xD1 */InstNotAvailable,
    /* 0xD2 */InstNotAvailable,
    /* 0xD3 */InstNotAvailable,
    /* 0xD4 */InstNotAvailable,
    /* 0xD5 */InstNotAvailable,
    /* 0xD6 */InstNotAvailable,
    /* 0xD7 */InstNotAvailable,
    /* 0xD8 */InstNotAvailable,
    /* 0xD9 */InstNotAvailable,
    /* 0xDA */InstNotAvailable,
    /* 0xDB */InstNotAvailable,
    /* 0xDC */InstNotAvailable,
    /* 0xDD */InstNotAvailable,
    /* 0xDE */InstNotAvailable,
    /* 0xDF */InstNotAvailable,
    /* 0xE0 */Fpu_Fsub_D8E0,
    /* 0xE1 */Fpu_Fsub_D8E0,
    /* 0xE2 */Fpu_Fsub_D8E0,
    /* 0xE3 */Fpu_Fsub_D8E0,
    /* 0xE4 */Fpu_Fsub_D8E0,
    /* 0xE5 */Fpu_Fsub_D8E0,
    /* 0xE6 */Fpu_Fsub_D8E0,
    /* 0xE7 */Fpu_Fsub_D8E0,
    /* 0xE8 */Fpu_Fsubr_D8E8,
    /* 0xE9 */Fpu_Fsubr_D8E8,
    /* 0xEA */Fpu_Fsubr_D8E8,
    /* 0xEB */Fpu_Fsubr_D8E8,
    /* 0xEC */Fpu_Fsubr_D8E8,
    /* 0xED */Fpu_Fsubr_D8E8,
    /* 0xEE */Fpu_Fsubr_D8E8,
    /* 0xEF */Fpu_Fsubr_D8E8,
    /* 0xF0 */Fpu_Fdiv_D8F0,
    /* 0xF1 */Fpu_Fdiv_D8F0,
    /* 0xF2 */Fpu_Fdiv_D8F0,
    /* 0xF3 */Fpu_Fdiv_D8F0,
    /* 0xF4 */Fpu_Fdiv_D8F0,
    /* 0xF5 */Fpu_Fdiv_D8F0,
    /* 0xF6 */Fpu_Fdiv_D8F0,
    /* 0xF7 */Fpu_Fdiv_D8F0,
    /* 0xF8 */InstNotAvailable,
    /* 0xF9 */InstNotAvailable,
    /* 0xFA */InstNotAvailable,
    /* 0xFB */InstNotAvailable,
    /* 0xFC */InstNotAvailable,
    /* 0xFD */InstNotAvailable,
    /* 0xFE */InstNotAvailable,
    /* 0xFF */InstNotAvailable,
};

static InstHandler Fpu_Ext_D9_Handlers[] = 
{
    /* 0x00 */InstNotAvailable,
    /* 0x08 */InstNotAvailable,
    /* 0x10 */InstNotAvailable,
    /* 0x18 */InstNotAvailable,
    /* 0x20 */InstNotAvailable,
    /* 0x28 */Fpu_Fldcw_D9_5,
    /* 0x30 */InstNotAvailable,
    /* 0x38 */Fpu_Fstcw_D9_7,

    /* 0xC0 */Fpu_Fld_D9C0,
    /* 0xC1 */Fpu_Fld_D9C0,
    /* 0xC2 */Fpu_Fld_D9C0,
    /* 0xC3 */Fpu_Fld_D9C0,
    /* 0xC4 */Fpu_Fld_D9C0,
    /* 0xC5 */Fpu_Fld_D9C0,
    /* 0xC6 */Fpu_Fld_D9C0,
    /* 0xC7 */Fpu_Fld_D9C0,
    /* 0xC8 */Fpu_Fxch_D9C8,
    /* 0xC9 */Fpu_Fxch_D9C8,
    /* 0xCA */Fpu_Fxch_D9C8,
    /* 0xCB */Fpu_Fxch_D9C8,
    /* 0xCC */Fpu_Fxch_D9C8,
    /* 0xCD */Fpu_Fxch_D9C8,
    /* 0xCE */Fpu_Fxch_D9C8,
    /* 0xCF */Fpu_Fxch_D9C8,
    /* 0xD0 */InstNotAvailable,
    /* 0xD1 */InstNotAvailable,
    /* 0xD2 */InstNotAvailable,
    /* 0xD3 */InstNotAvailable,
    /* 0xD4 */InstNotAvailable,
    /* 0xD5 */InstNotAvailable,
    /* 0xD6 */InstNotAvailable,
    /* 0xD7 */InstNotAvailable,
    /* 0xD8 */InstNotAvailable,
    /* 0xD9 */InstNotAvailable,
    /* 0xDA */InstNotAvailable,
    /* 0xDB */InstNotAvailable,
    /* 0xDC */InstNotAvailable,
    /* 0xDD */InstNotAvailable,
    /* 0xDE */InstNotAvailable,
    /* 0xDF */InstNotAvailable,
    /* 0xE0 */Fpu_Fchs_D9E0,
    /* 0xE1 */Fpu_Fabs_D9E1,
    /* 0xE2 */InstNotAvailable,
    /* 0xE3 */InstNotAvailable,
    /* 0xE4 */InstNotAvailable,
    /* 0xE5 */Fpu_Fxam_D9E5,
    /* 0xE6 */InstNotAvailable,
    /* 0xE7 */InstNotAvailable,
    /* 0xE8 */Fpu_Fld1_D9E8,
    /* 0xE9 */Fpu_Fldl2t_D9E9,
    /* 0xEA */Fpu_Fldl2e_D9EA,
    /* 0xEB */Fpu_Fldpi_D9EB,
    /* 0xEC */Fpu_Fldlg2_D9EC,
    /* 0xED */Fpu_Fldln2_D9ED,
    /* 0xEE */Fpu_Fldz_D9EE,
    /* 0xEF */InstNotAvailable,
    /* 0xF0 */InstNotAvailable,
    /* 0xF1 */Fpu_Fyl2x_D9F1,
    /* 0xF2 */Fpu_Fptan_D9F2,
    /* 0xF3 */InstNotAvailable,
    /* 0xF4 */InstNotAvailable,
    /* 0xF5 */Fpu_Fprem1_D9F5,
    /* 0xF6 */InstNotAvailable,
    /* 0xF7 */InstNotAvailable,
    /* 0xF8 */Fpu_Fprem_D9F8,
    /* 0xF9 */InstNotAvailable,
    /* 0xFA */Fpu_Fsqrt_D9FA,
    /* 0xFB */InstNotAvailable,
    /* 0xFC */Fpu_Frndint_D9FC,
    /* 0xFD */InstNotAvailable,
    /* 0xFE */InstNotAvailable,
    /* 0xFF */InstNotAvailable,
};

static InstHandler Fpu_Ext_DA_Handlers[] = 
{
    /* 0x00 */InstNotAvailable,
    /* 0x08 */InstNotAvailable,
    /* 0x10 */InstNotAvailable,
    /* 0x18 */InstNotAvailable,
    /* 0x20 */InstNotAvailable,
    /* 0x28 */InstNotAvailable,
    /* 0x30 */Fpu_Fidiv_DA_6,
    /* 0x38 */InstNotAvailable,

    /* 0xC0 */InstNotAvailable,
    /* 0xC1 */InstNotAvailable,
    /* 0xC2 */InstNotAvailable,
    /* 0xC3 */InstNotAvailable,
    /* 0xC4 */InstNotAvailable,
    /* 0xC5 */InstNotAvailable,
    /* 0xC6 */InstNotAvailable,
    /* 0xC7 */InstNotAvailable,
    /* 0xC8 */InstNotAvailable,
    /* 0xC9 */InstNotAvailable,
    /* 0xCA */InstNotAvailable,
    /* 0xCB */InstNotAvailable,
    /* 0xCC */InstNotAvailable,
    /* 0xCD */InstNotAvailable,
    /* 0xCE */InstNotAvailable,
    /* 0xCF */InstNotAvailable,
    /* 0xD0 */InstNotAvailable,
    /* 0xD1 */InstNotAvailable,
    /* 0xD2 */InstNotAvailable,
    /* 0xD3 */InstNotAvailable,
    /* 0xD4 */InstNotAvailable,
    /* 0xD5 */InstNotAvailable,
    /* 0xD6 */InstNotAvailable,
    /* 0xD7 */InstNotAvailable,
    /* 0xD8 */InstNotAvailable,
    /* 0xD9 */InstNotAvailable,
    /* 0xDA */InstNotAvailable,
    /* 0xDB */InstNotAvailable,
    /* 0xDC */InstNotAvailable,
    /* 0xDD */InstNotAvailable,
    /* 0xDE */InstNotAvailable,
    /* 0xDF */InstNotAvailable,
    /* 0xE0 */InstNotAvailable,
    /* 0xE1 */InstNotAvailable,
    /* 0xE2 */InstNotAvailable,
    /* 0xE3 */InstNotAvailable,
    /* 0xE4 */InstNotAvailable,
    /* 0xE5 */InstNotAvailable,
    /* 0xE6 */InstNotAvailable,
    /* 0xE7 */InstNotAvailable,
    /* 0xE8 */InstNotAvailable,
    /* 0xE9 */Fpu_Fucompp_DAE9,
    /* 0xEA */InstNotAvailable,
    /* 0xEB */InstNotAvailable,
    /* 0xEC */InstNotAvailable,
    /* 0xED */InstNotAvailable,
    /* 0xEE */InstNotAvailable,
    /* 0xEF */InstNotAvailable,
    /* 0xF0 */InstNotAvailable,
    /* 0xF1 */InstNotAvailable,
    /* 0xF2 */InstNotAvailable,
    /* 0xF3 */InstNotAvailable,
    /* 0xF4 */InstNotAvailable,
    /* 0xF5 */InstNotAvailable,
    /* 0xF6 */InstNotAvailable,
    /* 0xF7 */InstNotAvailable,
    /* 0xF8 */InstNotAvailable,
    /* 0xF9 */InstNotAvailable,
    /* 0xFA */InstNotAvailable,
    /* 0xFB */InstNotAvailable,
    /* 0xFC */InstNotAvailable,
    /* 0xFD */InstNotAvailable,
    /* 0xFE */InstNotAvailable,
    /* 0xFF */InstNotAvailable,
};

static InstHandler Fpu_Ext_DB_Handlers[] = 
{
    /* 0x00 */Fpu_Fild32_DB_0,
    /* 0x08 */InstNotAvailable,
    /* 0x10 */InstNotAvailable,
    /* 0x18 */Fpu_Fistp32_DB_3,
    /* 0x20 */InstNotAvailable,
    /* 0x28 */Fpu_Fld80_DB_5,
    /* 0x30 */InstNotAvailable,
    /* 0x38 */InstNotAvailable,

    /* 0xC0 */InstNotAvailable,
    /* 0xC1 */InstNotAvailable,
    /* 0xC2 */InstNotAvailable,
    /* 0xC3 */InstNotAvailable,
    /* 0xC4 */InstNotAvailable,
    /* 0xC5 */InstNotAvailable,
    /* 0xC6 */InstNotAvailable,
    /* 0xC7 */InstNotAvailable,
    /* 0xC8 */InstNotAvailable,
    /* 0xC9 */InstNotAvailable,
    /* 0xCA */InstNotAvailable,
    /* 0xCB */InstNotAvailable,
    /* 0xCC */InstNotAvailable,
    /* 0xCD */InstNotAvailable,
    /* 0xCE */InstNotAvailable,
    /* 0xCF */InstNotAvailable,
    /* 0xD0 */InstNotAvailable,
    /* 0xD1 */InstNotAvailable,
    /* 0xD2 */InstNotAvailable,
    /* 0xD3 */InstNotAvailable,
    /* 0xD4 */InstNotAvailable,
    /* 0xD5 */InstNotAvailable,
    /* 0xD6 */InstNotAvailable,
    /* 0xD7 */InstNotAvailable,
    /* 0xD8 */InstNotAvailable,
    /* 0xD9 */InstNotAvailable,
    /* 0xDA */InstNotAvailable,
    /* 0xDB */InstNotAvailable,
    /* 0xDC */InstNotAvailable,
    /* 0xDD */InstNotAvailable,
    /* 0xDE */InstNotAvailable,
    /* 0xDF */InstNotAvailable,
    /* 0xE0 */InstNotAvailable,
    /* 0xE1 */InstNotAvailable,
    /* 0xE2 */Fpu_Fnclex_DBE2,
    /* 0xE3 */Fpu_Fninit_DBE3,
    /* 0xE4 */InstNotAvailable,
    /* 0xE5 */InstNotAvailable,
    /* 0xE6 */InstNotAvailable,
    /* 0xE7 */InstNotAvailable,
    /* 0xE8 */InstNotAvailable,
    /* 0xE9 */InstNotAvailable,
    /* 0xEA */InstNotAvailable,
    /* 0xEB */InstNotAvailable,
    /* 0xEC */InstNotAvailable,
    /* 0xED */InstNotAvailable,
    /* 0xEE */InstNotAvailable,
    /* 0xEF */InstNotAvailable,
    /* 0xF0 */InstNotAvailable,
    /* 0xF1 */InstNotAvailable,
    /* 0xF2 */InstNotAvailable,
    /* 0xF3 */InstNotAvailable,
    /* 0xF4 */InstNotAvailable,
    /* 0xF5 */InstNotAvailable,
    /* 0xF6 */InstNotAvailable,
    /* 0xF7 */InstNotAvailable,
    /* 0xF8 */InstNotAvailable,
    /* 0xF9 */InstNotAvailable,
    /* 0xFA */InstNotAvailable,
    /* 0xFB */InstNotAvailable,
    /* 0xFC */InstNotAvailable,
    /* 0xFD */InstNotAvailable,
    /* 0xFE */InstNotAvailable,
    /* 0xFF */InstNotAvailable,
};

static InstHandler Fpu_Ext_DC_Handlers[] = 
{
    /* 0x00 */Fpu_Fadd_DC_0,
    /* 0x08 */Fpu_Fmul_DC_1,
    /* 0x10 */Fpu_Fcom_DC_2,
    /* 0x18 */Fpu_Fcomp_DC_3,
    /* 0x20 */Fpu_Fsub_DC_4,
    /* 0x28 */Fpu_Fsubr_DC_5,
    /* 0x30 */Fpu_Fdiv_DC_6,
    /* 0x38 */Fpu_Fdivr_DC_7,

    /* 0xC0 */Fpu_Fadd_DCC0,
    /* 0xC1 */Fpu_Fadd_DCC0,
    /* 0xC2 */Fpu_Fadd_DCC0,
    /* 0xC3 */Fpu_Fadd_DCC0,
    /* 0xC4 */Fpu_Fadd_DCC0,
    /* 0xC5 */Fpu_Fadd_DCC0,
    /* 0xC6 */Fpu_Fadd_DCC0,
    /* 0xC7 */Fpu_Fadd_DCC0,
    /* 0xC8 */Fpu_Fmul_DCC8,
    /* 0xC9 */Fpu_Fmul_DCC8,
    /* 0xCA */Fpu_Fmul_DCC8,
    /* 0xCB */Fpu_Fmul_DCC8,
    /* 0xCC */Fpu_Fmul_DCC8,
    /* 0xCD */Fpu_Fmul_DCC8,
    /* 0xCE */Fpu_Fmul_DCC8,
    /* 0xCF */Fpu_Fmul_DCC8,
    /* 0xD0 */InstNotAvailable,
    /* 0xD1 */InstNotAvailable,
    /* 0xD2 */InstNotAvailable,
    /* 0xD3 */InstNotAvailable,
    /* 0xD4 */InstNotAvailable,
    /* 0xD5 */InstNotAvailable,
    /* 0xD6 */InstNotAvailable,
    /* 0xD7 */InstNotAvailable,
    /* 0xD8 */InstNotAvailable,
    /* 0xD9 */InstNotAvailable,
    /* 0xDA */InstNotAvailable,
    /* 0xDB */InstNotAvailable,
    /* 0xDC */InstNotAvailable,
    /* 0xDD */InstNotAvailable,
    /* 0xDE */InstNotAvailable,
    /* 0xDF */InstNotAvailable,
    /* 0xE0 */Fpu_Fsubr_DCE0,
    /* 0xE1 */Fpu_Fsubr_DCE0,
    /* 0xE2 */Fpu_Fsubr_DCE0,
    /* 0xE3 */Fpu_Fsubr_DCE0,
    /* 0xE4 */Fpu_Fsubr_DCE0,
    /* 0xE5 */Fpu_Fsubr_DCE0,
    /* 0xE6 */Fpu_Fsubr_DCE0,
    /* 0xE7 */Fpu_Fsubr_DCE0,
    /* 0xE8 */InstNotAvailable,
    /* 0xE9 */InstNotAvailable,
    /* 0xEA */InstNotAvailable,
    /* 0xEB */InstNotAvailable,
    /* 0xEC */InstNotAvailable,
    /* 0xED */InstNotAvailable,
    /* 0xEE */InstNotAvailable,
    /* 0xEF */InstNotAvailable,
    /* 0xF0 */Fpu_Fdivr_DCF0,
    /* 0xF1 */Fpu_Fdivr_DCF0,
    /* 0xF2 */Fpu_Fdivr_DCF0,
    /* 0xF3 */Fpu_Fdivr_DCF0,
    /* 0xF4 */Fpu_Fdivr_DCF0,
    /* 0xF5 */Fpu_Fdivr_DCF0,
    /* 0xF6 */Fpu_Fdivr_DCF0,
    /* 0xF7 */Fpu_Fdivr_DCF0,
    /* 0xF8 */Fpu_Fdiv_DCF8,
    /* 0xF9 */Fpu_Fdiv_DCF8,
    /* 0xFA */Fpu_Fdiv_DCF8,
    /* 0xFB */Fpu_Fdiv_DCF8,
    /* 0xFC */Fpu_Fdiv_DCF8,
    /* 0xFD */Fpu_Fdiv_DCF8,
    /* 0xFE */Fpu_Fdiv_DCF8,
    /* 0xFF */Fpu_Fdiv_DCF8,
};

static InstHandler Fpu_Ext_DD_Handlers[] = 
{
    /* 0x00 */Fpu_Fld64fp_DD_0,
    /* 0x08 */InstNotAvailable,
    /* 0x10 */Fpu_Fst64fp_DD_2,
    /* 0x18 */Fpu_Fstp64fp_DD_3,
    /* 0x20 */InstNotAvailable,
    /* 0x28 */InstNotAvailable,
    /* 0x30 */InstNotAvailable,
    /* 0x38 */Fpu_Fstsw_DD_7,

    /* 0xC0 */InstNotAvailable,
    /* 0xC1 */InstNotAvailable,
    /* 0xC2 */InstNotAvailable,
    /* 0xC3 */InstNotAvailable,
    /* 0xC4 */InstNotAvailable,
    /* 0xC5 */InstNotAvailable,
    /* 0xC6 */InstNotAvailable,
    /* 0xC7 */InstNotAvailable,
    /* 0xC8 */InstNotAvailable,
    /* 0xC9 */InstNotAvailable,
    /* 0xCA */InstNotAvailable,
    /* 0xCB */InstNotAvailable,
    /* 0xCC */InstNotAvailable,
    /* 0xCD */InstNotAvailable,
    /* 0xCE */InstNotAvailable,
    /* 0xCF */InstNotAvailable,
    /* 0xD0 */InstNotAvailable,
    /* 0xD1 */InstNotAvailable,
    /* 0xD2 */InstNotAvailable,
    /* 0xD3 */InstNotAvailable,
    /* 0xD4 */InstNotAvailable,
    /* 0xD5 */InstNotAvailable,
    /* 0xD6 */InstNotAvailable,
    /* 0xD7 */InstNotAvailable,
    /* 0xD8 */Fpu_Fstp_DDD8,
    /* 0xD9 */Fpu_Fstp_DDD8,
    /* 0xDA */Fpu_Fstp_DDD8,
    /* 0xDB */Fpu_Fstp_DDD8,
    /* 0xDC */Fpu_Fstp_DDD8,
    /* 0xDD */Fpu_Fstp_DDD8,
    /* 0xDE */Fpu_Fstp_DDD8,
    /* 0xDF */Fpu_Fstp_DDD8,
    /* 0xE0 */InstNotAvailable,
    /* 0xE1 */InstNotAvailable,
    /* 0xE2 */InstNotAvailable,
    /* 0xE3 */InstNotAvailable,
    /* 0xE4 */InstNotAvailable,
    /* 0xE5 */InstNotAvailable,
    /* 0xE6 */InstNotAvailable,
    /* 0xE7 */InstNotAvailable,
    /* 0xE8 */InstNotAvailable,
    /* 0xE9 */InstNotAvailable,
    /* 0xEA */InstNotAvailable,
    /* 0xEB */InstNotAvailable,
    /* 0xEC */InstNotAvailable,
    /* 0xED */InstNotAvailable,
    /* 0xEE */InstNotAvailable,
    /* 0xEF */InstNotAvailable,
    /* 0xF0 */InstNotAvailable,
    /* 0xF1 */InstNotAvailable,
    /* 0xF2 */InstNotAvailable,
    /* 0xF3 */InstNotAvailable,
    /* 0xF4 */InstNotAvailable,
    /* 0xF5 */InstNotAvailable,
    /* 0xF6 */InstNotAvailable,
    /* 0xF7 */InstNotAvailable,
    /* 0xF8 */InstNotAvailable,
    /* 0xF9 */InstNotAvailable,
    /* 0xFA */InstNotAvailable,
    /* 0xFB */InstNotAvailable,
    /* 0xFC */InstNotAvailable,
    /* 0xFD */InstNotAvailable,
    /* 0xFE */InstNotAvailable,
    /* 0xFF */InstNotAvailable,
};

static InstHandler Fpu_Ext_DE_Handlers[] = 
{
    /* 0x00 */InstNotAvailable,
    /* 0x08 */InstNotAvailable,
    /* 0x10 */InstNotAvailable,
    /* 0x18 */InstNotAvailable,
    /* 0x20 */InstNotAvailable,
    /* 0x28 */InstNotAvailable,
    /* 0x30 */Fpu_Fidiv_DE_6,
    /* 0x38 */InstNotAvailable,

    /* 0xC0 */Fpu_Faddp_DEC0,
    /* 0xC1 */Fpu_Faddp_DEC0,
    /* 0xC2 */Fpu_Faddp_DEC0,
    /* 0xC3 */Fpu_Faddp_DEC0,
    /* 0xC4 */Fpu_Faddp_DEC0,
    /* 0xC5 */Fpu_Faddp_DEC0,
    /* 0xC6 */Fpu_Faddp_DEC0,
    /* 0xC7 */Fpu_Faddp_DEC0,
    /* 0xC8 */Fpu_Fmulp_DEC8,
    /* 0xC9 */Fpu_Fmulp_DEC8,
    /* 0xCA */Fpu_Fmulp_DEC8,
    /* 0xCB */Fpu_Fmulp_DEC8,
    /* 0xCC */Fpu_Fmulp_DEC8,
    /* 0xCD */Fpu_Fmulp_DEC8,
    /* 0xCE */Fpu_Fmulp_DEC8,
    /* 0xCF */Fpu_Fmulp_DEC8,
    /* 0xD0 */InstNotAvailable,
    /* 0xD1 */InstNotAvailable,
    /* 0xD2 */InstNotAvailable,
    /* 0xD3 */InstNotAvailable,
    /* 0xD4 */InstNotAvailable,
    /* 0xD5 */InstNotAvailable,
    /* 0xD6 */InstNotAvailable,
    /* 0xD7 */InstNotAvailable,
    /* 0xD8 */InstNotAvailable,
    /* 0xD9 */InstNotAvailable,
    /* 0xDA */InstNotAvailable,
    /* 0xDB */InstNotAvailable,
    /* 0xDC */InstNotAvailable,
    /* 0xDD */InstNotAvailable,
    /* 0xDE */InstNotAvailable,
    /* 0xDF */InstNotAvailable,
    /* 0xE0 */Fpu_Fsubrp_DEE0,
    /* 0xE1 */Fpu_Fsubrp_DEE0,
    /* 0xE2 */Fpu_Fsubrp_DEE0,
    /* 0xE3 */Fpu_Fsubrp_DEE0,
    /* 0xE4 */Fpu_Fsubrp_DEE0,
    /* 0xE5 */Fpu_Fsubrp_DEE0,
    /* 0xE6 */Fpu_Fsubrp_DEE0,
    /* 0xE7 */Fpu_Fsubrp_DEE0,
    /* 0xE8 */Fpu_Fsubp_DEE8,
    /* 0xE9 */Fpu_Fsubp_DEE8,
    /* 0xEA */Fpu_Fsubp_DEE8,
    /* 0xEB */Fpu_Fsubp_DEE8,
    /* 0xEC */Fpu_Fsubp_DEE8,
    /* 0xED */Fpu_Fsubp_DEE8,
    /* 0xEE */Fpu_Fsubp_DEE8,
    /* 0xEF */Fpu_Fsubp_DEE8,
    /* 0xF0 */InstNotAvailable,
    /* 0xF1 */InstNotAvailable,
    /* 0xF2 */InstNotAvailable,
    /* 0xF3 */InstNotAvailable,
    /* 0xF4 */InstNotAvailable,
    /* 0xF5 */InstNotAvailable,
    /* 0xF6 */InstNotAvailable,
    /* 0xF7 */InstNotAvailable,
    /* 0xF8 */Fpu_Fdivp_DEF8,
    /* 0xF9 */Fpu_Fdivp_DEF8,
    /* 0xFA */Fpu_Fdivp_DEF8,
    /* 0xFB */Fpu_Fdivp_DEF8,
    /* 0xFC */Fpu_Fdivp_DEF8,
    /* 0xFD */Fpu_Fdivp_DEF8,
    /* 0xFE */Fpu_Fdivp_DEF8,
    /* 0xFF */Fpu_Fdivp_DEF8,
};

static InstHandler Fpu_Ext_DF_Handlers[] = 
{
    /* 0x00 */Fpu_Fild16_DF_0,
    /* 0x08 */InstNotAvailable,
    /* 0x10 */InstNotAvailable,
    /* 0x18 */InstNotAvailable,
    /* 0x20 */InstNotAvailable,
    /* 0x28 */Fpu_Fild64_DF_5,
    /* 0x30 */InstNotAvailable,
    /* 0x38 */Fpu_Fistp64int_DF_7,

    /* 0xC0 */InstNotAvailable,
    /* 0xC1 */InstNotAvailable,
    /* 0xC2 */InstNotAvailable,
    /* 0xC3 */InstNotAvailable,
    /* 0xC4 */InstNotAvailable,
    /* 0xC5 */InstNotAvailable,
    /* 0xC6 */InstNotAvailable,
    /* 0xC7 */InstNotAvailable,
    /* 0xC8 */InstNotAvailable,
    /* 0xC9 */InstNotAvailable,
    /* 0xCA */InstNotAvailable,
    /* 0xCB */InstNotAvailable,
    /* 0xCC */InstNotAvailable,
    /* 0xCD */InstNotAvailable,
    /* 0xCE */InstNotAvailable,
    /* 0xCF */InstNotAvailable,
    /* 0xD0 */InstNotAvailable,
    /* 0xD1 */InstNotAvailable,
    /* 0xD2 */InstNotAvailable,
    /* 0xD3 */InstNotAvailable,
    /* 0xD4 */InstNotAvailable,
    /* 0xD5 */InstNotAvailable,
    /* 0xD6 */InstNotAvailable,
    /* 0xD7 */InstNotAvailable,
    /* 0xD8 */InstNotAvailable,
    /* 0xD9 */InstNotAvailable,
    /* 0xDA */InstNotAvailable,
    /* 0xDB */InstNotAvailable,
    /* 0xDC */InstNotAvailable,
    /* 0xDD */InstNotAvailable,
    /* 0xDE */InstNotAvailable,
    /* 0xDF */InstNotAvailable,
    /* 0xE0 */Fpu_Fstsw_DFE0,
    /* 0xE1 */InstNotAvailable,
    /* 0xE2 */InstNotAvailable,
    /* 0xE3 */InstNotAvailable,
    /* 0xE4 */InstNotAvailable,
    /* 0xE5 */InstNotAvailable,
    /* 0xE6 */InstNotAvailable,
    /* 0xE7 */InstNotAvailable,
    /* 0xE8 */InstNotAvailable,
    /* 0xE9 */InstNotAvailable,
    /* 0xEA */InstNotAvailable,
    /* 0xEB */InstNotAvailable,
    /* 0xEC */InstNotAvailable,
    /* 0xED */InstNotAvailable,
    /* 0xEE */InstNotAvailable,
    /* 0xEF */InstNotAvailable,
    /* 0xF0 */InstNotAvailable,
    /* 0xF1 */InstNotAvailable,
    /* 0xF2 */InstNotAvailable,
    /* 0xF3 */InstNotAvailable,
    /* 0xF4 */InstNotAvailable,
    /* 0xF5 */InstNotAvailable,
    /* 0xF6 */InstNotAvailable,
    /* 0xF7 */InstNotAvailable,
    /* 0xF8 */InstNotAvailable,
    /* 0xF9 */InstNotAvailable,
    /* 0xFA */InstNotAvailable,
    /* 0xFB */InstNotAvailable,
    /* 0xFC */InstNotAvailable,
    /* 0xFD */InstNotAvailable,
    /* 0xFE */InstNotAvailable,
    /* 0xFF */InstNotAvailable,
};

static InstHandler *Fpu_Handlers[] = 
{
    Fpu_Ext_D8_Handlers,
    Fpu_Ext_D9_Handlers,
    Fpu_Ext_DA_Handlers,
    Fpu_Ext_DB_Handlers,
    Fpu_Ext_DC_Handlers,
    Fpu_Ext_DD_Handlers,
    Fpu_Ext_DE_Handlers,
    Fpu_Ext_DF_Handlers,
};

LxResult Fpu_Ext(Processor *cpu, const Instruction *inst)
{
    Assert(inst->Main.Inst.Opcode >= 0xd8 && inst->Main.Inst.Opcode <= 0xdf);
    int index = inst->Aux.opcode < 0xc0 ? MASK_MODRM_REG(inst->Aux.opcode) : inst->Aux.opcode - 0xb8;
    Assert(index >= 0 && index < 0x48);
    return Fpu_Handlers[inst->Main.Inst.Opcode - 0xd8][index](cpu, inst);
}

END_NAMESPACE_LOCHSEMU()